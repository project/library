<?php

namespace Drupal\library\Event;

use Drupal\library\Entity\LibraryTransaction;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Returns available action.
 */
class ActionEvent extends Event {

  /**
   * Library transaction.
   *
   * @var \Drupal\library\Entity\LibraryTransaction
   */
  private $transaction;

  /**
   * Constructor.
   */
  public function __construct(LibraryTransaction $transaction) {
    $this->transaction = $transaction;
  }

  /**
   * Returns the kernel in which this event was thrown.
   *
   * @return \Drupal\library\Entity\LibraryTransaction
   *   Transaction.
   */
  public function getTransaction(): LibraryTransaction {
    return $this->transaction;
  }

}
