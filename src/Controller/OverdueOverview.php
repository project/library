<?php

namespace Drupal\library\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\library\LibraryItemInterface;

/**
 * Overdue overview controller.
 *
 * @package Drupal\library\Controller
 */
class OverdueOverview extends ControllerBase {

  /**
   * List.
   *
   * @return array
   *   Return Hello string.
   */
  public function listing(): array {
    // @todo Inject.
    $renderer = \Drupal::service('renderer');
    $manager = \Drupal::entityTypeManager();
    $itemStorage = $manager->getStorage('library_item');
    $transactionStorage = $manager->getStorage('library_transaction');
    $nodeStorage = $manager->getStorage('node');
    $userStorage = $manager->getStorage('user');
    $dateFormatter = \Drupal::service('date.formatter');

    $data['elements'] = [
      '#type' => 'table',
      '#title' => $this->t('Item history'),
      '#header' => ['Item', 'Patron', 'Due Date', 'Last comment', 'Edit'],
    ];

    $items = $itemStorage->getQuery()
      ->condition('library_status', LibraryItemInterface::ITEM_UNAVAILABLE)
      ->execute();

    $loadedItems = $itemStorage->loadMultiple($items);

    foreach ($loadedItems as $item) {
      /** @var \Drupal\library\Entity\LibraryItem $item */
      $format_title = '';
      if ($item->get('nid')->getValue()) {
        /** @var \Drupal\node\Entity\Node $node */
        $node = $nodeStorage->load($item->get('nid')->getValue()[0]['target_id']);
        $label = $node->getTitle();

        if ($item->get('barcode')->value) {
          $label .= ' (' . $item->get('barcode')->value . ')';
        }
        $format_title = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => Url::fromRoute('entity.node.canonical', ['node' => $node->id()]),
        ];
        $format_title = $renderer->render($format_title);

      }

      $transaction = $item->getLatestTransactionDue();

      if ($transaction && count($transaction) == 1) {
        /** @var \Drupal\library\Entity\LibraryTransaction $loadedTransaction */
        $loadedTransaction = $transactionStorage->load(array_pop($transaction));
        $patronName = '';
        if ($loadedTransaction->get('uid')->getValue()) {
          $patron = $userStorage->load($loadedTransaction->get('uid')->getValue()[0]['target_id']);
          if ($patron) {
            $patronName = [
              '#type' => 'link',
              '#title' => $patron->getDisplayName(),
              '#url' => Url::fromRoute('entity.user.canonical', ['user' => $patron->id()]),
            ];
            $patronName = $renderer->render($patronName);

            $patronId = $patron->id();
          }
        }

        $due = '';
        if ($loadedTransaction->getDueDate() > 0) {
          $due = $dateFormatter->format($loadedTransaction->getDueDate(), 'short');
        }

        $editLink = [
          '#type' => 'link',
          '#title' => $this->t('Add note'),
          '#url' => Url::fromRoute('library.edit_transaction', ['transaction' => $loadedTransaction->id()]),
        ];

        $data['elements']['#rows'][$patronId . '_' . $item->id()] = [
          $format_title,
          $patronName,
          $due,
          $this->formatMarkup($loadedTransaction->get('notes')->value),
          $renderer->render($editLink),
        ];
        ksort($data['elements']['#rows']);
      }
    }

    return $data;
  }

  /**
   * Format markup.
   *
   * @param string $data
   *   Data for render array.
   *
   * @return string
   *   Formatted HTML.
   */
  private function formatMarkup($data): string {
    $escaped = ['#markup' => nl2br($data)];
    return \Drupal::service('renderer')->render($escaped);
  }

}
