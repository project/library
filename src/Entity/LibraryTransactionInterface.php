<?php

namespace Drupal\library\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Library transaction entities.
 */
interface LibraryTransactionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Library transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Library transaction.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Library transaction creation timestamp.
   *
   * @param int $timestamp
   *   The Library transaction creation timestamp.
   *
   * @return \Drupal\library\Entity\LibraryTransactionInterface
   *   The called Library transaction entity.
   */
  public function setCreatedTime($timestamp): LibraryTransactionInterface;

  /**
   * Get Node ID.
   *
   * @return int|null
   *   Node ID.
   */
  public function getNid(): ?int;

  /**
   * Get Patron ID.
   *
   * @return int|null
   *   User ID.
   */
  public function getPatron(): ?int;

  /**
   * Get due date.
   *
   * @return int|null
   *   Due date as timestamp.
   */
  public function getDueDate(): ?int;

}
