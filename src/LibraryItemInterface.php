<?php

namespace Drupal\library;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Library item entities.
 */
interface LibraryItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  public const IN_CIRCULATION = 0;
  public const REFERENCE_ONLY = 1;

  public const ITEM_AVAILABLE = 0;
  public const ITEM_UNAVAILABLE = 1;

}
