INTRODUCTION
------------

This module allows users to manage nodes as assets that may be available or
unavailable. Users may create custom content types and then add those content
types to the library.

It supports multiple copies of a library item associated with one node, and each
copy may be individually made available or unavailable.

The module allows administrators to define their own library actions. Library
actions may make an item available, unavailable, or not change an item's status.
Every transaction is associated with a Drupal user.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

After installing the library module with composer, the site administrator needs
to enable library functionality on at least one content type.  To add library
functionality to a content type, modify the content type settings at
admin/structure/types/manage/<type>.  Select 'Yes' for Library Item under
Workflow settings.

CONFIGURATION
-------------

Library module settings may be configured at admin/config/workflow/library.
No options are enabled by default, so to see any additional functionality, users
will need to modify the settings. Settings include:
* Use unique identifiers (barcodes) on library items
* Display specific actions as options
* Display terms from specific vocabularies in library lists
*  Modify status text (e.g. "Available" vs. "IN")
* Enable due date functionality
* Add/rename library actions

The library module comes with two default library actions: 'Check In' and
'Check Out'.  You may rename these and/or create new library actions at
admin/structure/library_action. Each library action has it's own permission, so
access control can be very fine-grained.
